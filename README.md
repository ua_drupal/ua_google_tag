# UA Google Tag Drupal module

## End of life (2023-11-15)

As of November 15, 2023, UA Quickstart (Quickstart 1) and all of its associated repositories will no longer supported by the Arizona Digital team.  Any website still using Quickstart 1 after the end-of-life date should be migrated to Quickstart 2 as soon as possible.

See the [main UA Quickstart repository](https://bitbucket.org/ua_drupal/ua_quickstart/src/7.x-1.x/README.md) for more information.

## Overview

Pre-configures the contrib Drupal [Google Tag](https://www.drupal.org/project/google_tag) to work with the University of Arizona's [Google Analytics](http://www.google.com/tagmanager/).

## Requirements


### Dependencies
This module requires the following modules and libraries to be available.

#### Packaged Dependencies
When this module is used as part of a Drupal distribution (such as [UA Quickstart](https://bitbucket.org/ua_drupal/ua_quickstart)), the following dependencies will be automatically packaged with the distribution via drush make.

- [Google Tag Manager module](https://www.drupal.org/project/google_tag)

#### Other Dependencies
When this module is used outside of a Drupal distribution, the packaged dependencies above as well as these additional module dependencies will need to be installed manually.

- [Features](https://www.drupal.org/project/features)
- [Strongarm](https://www.drupal.org/project/strongarm)
